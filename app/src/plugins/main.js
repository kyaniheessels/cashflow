import Movue from 'movue'
import * as mobx from 'mobx'

export default ({ app, router, Vue }) => {
  Vue.use(Movue, mobx)
}
