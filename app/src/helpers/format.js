export function showAmount (v) {
  return '$ ' + showNumber(v)
}

export function showNumber (v) {
  if (!v || !v.toString) {
    return 0
  }

  var parts = v.toString().split('.')
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  return parts.join('.')
}
