import {GunStore} from './gun-mobx'
import {observable, action, autorun, toJS, computed} from 'mobx' /*  action, observe */
import professions from './professions.json'
import beginningInvestmentPortfolios from './beginning-investment-portfolios.json'
import doodads from './doodads.json'
import dreams from './dreams-fast-track.json'
import otherFastTrack from './other-fast-track.json'

function checkNumber (v) {
  v = parseFloat(v)
  if (isNaN(v)) {
    v = 0
  }
  return v
}

class Store extends GunStore {
  default = {
    professions: professions,
    beginningInvestmentPortfolios: beginningInvestmentPortfolios,
    doodads: doodads,
    dreams: dreams,
    otherFastTrack: otherFastTrack
  }

  @observable showDebugStatement = false

  @observable cashflowVersion = '101'
  @observable cash = 0
  @observable salary = 0
  @observable taxes = 0
  @observable homeMortgagePayment = 0
  @observable schoolLoanPayment = 0
  @observable carPayment = 0
  @observable creditcardPayment = 0
  @observable retailPayment = 0
  @observable otherExpenses = 0
  @observable numberOfChildren = 0
  @observable perChildExpense = 0
  @observable homeMortgage = 0
  @observable schoolLoans = 0
  @observable carLoans = 0
  @observable creditcards = 0
  @observable retailDebt = 0
  @observable boatLoan = 0
  @observable bankLoan = 0
  @observable dream = 0
  @observable oldState = {}
  @observable cashFastTrackExtra = 0
  @observable franchiseAmount = 1

  @observable stocks = {}
  @observable realEstates = {}
  @observable businesses = {}
  @observable businessesFastTrack = {}
  @observable dreams = {}
  @observable liabilities = {}
  @observable otherAssets = {}
  @observable dreamsBought = {}

  @observable changeHack = 0.1

  @action.bound setShowDebugStatement (v) {
    this.showDebugStatement = v
  }
  @action.bound setCashflowVersion (v) {
    this.cashflowVersion = v
  }
  @action.bound setSalary (v) {
    this.salary = v
  }
  @action.bound setTaxes (v) {
    this.taxes = v
  }
  @action.bound setHomeMortgagePayment (v) {
    this.homeMortgagePayment = v
  }
  @action.bound setSchoolLoanPayment (v) {
    this.schoolLoanPayment = v
  }
  @action.bound addSchoolLoanPayment (v) {
    this.schoolLoanPayment += checkNumber(v)
  }
  @action.bound setCarPayment (v) {
    this.carPayment = v
  }
  @action.bound addCarPayment (v) {
    this.carPayment += checkNumber(v)
  }
  @action.bound setCreditcardPayment (v) {
    this.creditcardPayment = v
  }
  @action.bound setRetailPayment (v) {
    this.retailPayment = v
  }
  @action.bound setOtherExpenses (v) {
    this.otherExpenses = v
  }
  @action.bound setPerChildExpense (v) {
    this.perChildExpense = v
  }
  @action.bound setHomeMortgage (v) {
    this.homeMortgage = v
  }
  @action.bound setSchoolLoans (v) {
    this.schoolLoans = v
  }
  @action.bound addSchoolLoans (v) {
    this.schoolLoans += checkNumber(v)
  }
  @action.bound setCarLoans (v) {
    this.carLoans = v
  }
  @action.bound addCarLoans (v) {
    this.carLoans += checkNumber(v)
  }
  @action.bound setCreditcards (v) {
    this.creditcards = v
  }
  @action.bound setRetailDebt (v) {
    this.retailDebt = v
  }
  @action.bound setCash (v) {
    this.cash = checkNumber(v)
  }
  @action.bound setDream (v) {
    this.dream = (v)
  }
  @action.bound addCash (v) {
    this.cash += checkNumber(v)
  }
  @action.bound addCashFastTrack (v) {
    this.cashFastTrackExtra += checkNumber(v)
  }
  @action.bound increaseOtherExpenses (v) {
    this.otherExpenses += checkNumber(v)
  }
  @action.bound increaseCreditcards (v) {
    this.creditcards += checkNumber(v)
  }
  @action.bound increaseCreditcardPayment (v) {
    this.creditcardPayment += checkNumber(v)
  }
  @action.bound addBankLoan (v) {
    this.bankLoan += v
  }
  @action.bound addBoatLoan (v) {
    this.boatLoan += v
  }
  @action.bound addChild (v) {
    this.numberOfChildren += v
  }
  @action.bound removeChild (v) {
    this.numberOfChildren -= v
  }

  /*
  --------------------------------------------------------------------------
  */

  @action.bound addRealEstate (v) {
    let uuid = generateUuid()
    this.realEstates[uuid] = v
    this.changeHack = Math.random()
  }

  @action.bound sellRealestate (uuid, amount, higher) {
    let that = this
    Object.keys(this.realEstates).forEach(function (key) {
      if (key === uuid) {
        let am = parseInt(amount, 10)
        if (higher) {
          am = parseInt(that.realEstates[key].cost, 10) + am
        }
        that.addCash(am - parseInt(that.realEstates[key].mortgage, 10))
        delete that.realEstates[key]
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound realestateAddCashflow (uuid, amount) {
    let that = this
    Object.keys(this.realEstates).forEach(function (key) {
      if (key === uuid) {
        let am = parseInt(amount, 10)
        that.realEstates[key].cashFlow += am
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound sellRealestatePart (uuid, units, amount) {
    let that = this
    Object.keys(this.realEstates).forEach(function (key) {
      if (key === uuid) {
        let am = parseInt(amount, 10)
        let fact = units / that.realEstates[key].units
        let morg = that.realEstates[key].mortgage * fact
        that.addCash(am - parseInt(morg, 10))
        that.realEstates[key].units -= units
        that.realEstates[key].cashFlow *= (1 - fact)
        that.realEstates[key].cost *= (1 - fact)
        that.realEstates[key].downPay *= (1 - fact)
        that.realEstates[key].mortgage *= (1 - fact)
        if (that.realEstates[key].units <= 0) {
          delete that.realEstates[key]
        }
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound addStock (v) {
    let that = this
    v.numberOfShares = parseInt(v.numberOfShares, 10)
    if (v.numberOfShares > 0) {
      let uuid = generateUuid()
      this.stocks[uuid] = v
      this.changeHack = Math.random()
      let am = v.numberOfShares * parseInt(v.costPerShare, 10)
      that.addCash(-am)
    }
    if (v.numberOfShares < 0) {
      let haveShares = this.getTotalUnitsOfStock(v.type)
      v.numberOfShares = -v.numberOfShares
      if (v.numberOfShares > haveShares) {
        v.numberOfShares = haveShares
      }
      while (v.numberOfShares > 0) {
        Object.keys(this.stocks).forEach(function (key) {
          if (that.stocks[key].type === v.type) {
            let a = v.numberOfShares
            if (that.stocks[key].numberOfShares < a) {
              a = that.stocks[key].numberOfShares
            }
            let am = a * parseInt(v.costPerShare, 10)
            that.addCash(am)
            if (a === that.stocks[key].numberOfShares) {
              delete that.stocks[key]
            } else {
              that.stocks[key].numberOfShares -= a
            }
            v.numberOfShares -= a
          }
        })
      }
    }
  }

  @action.bound addStockShort (v) {
    let that = this
    v.numberOfSharesShort = parseInt(v.numberOfSharesShort, 10)
    if (v.numberOfSharesShort > 0) {
      let uuid = generateUuid()
      this.stocks[uuid] = v
      this.changeHack = Math.random()
    }
    if (v.numberOfSharesShort < 0) {
      let haveShares = this.getTotalUnitsOfStockShort(v.type)
      v.numberOfSharesShort = -v.numberOfSharesShort
      if (v.numberOfSharesShort > haveShares) {
        v.numberOfSharesShort = haveShares
      }
      while (v.numberOfSharesShort > 0) {
        Object.keys(this.stocks).forEach(function (key) {
          if (that.stocks[key].type === v.type) {
            let a = v.numberOfSharesShort
            if (that.stocks[key].numberOfSharesShort < a) {
              a = that.stocks[key].numberOfSharesShort
            }
            let am = a * (that.stocks[key].costPerShare - parseInt(v.costPerShare, 10))
            that.addCash(am)
            if (a === that.stocks[key].numberOfSharesShort) {
              delete that.stocks[key]
            } else {
              that.stocks[key].numberOfSharesShort -= a
            }
            v.numberOfSharesShort -= a
          }
        })
      }
    }
  }

  @action.bound addStockCall (v) {
    let that = this
    v.numberOfSharesCall = parseInt(v.numberOfSharesCall, 10)
    if (v.numberOfSharesCall > 0) {
      let uuid = generateUuid()
      this.stocks[uuid] = v
      this.changeHack = Math.random()
      let am = v.numberOfSharesCall * parseInt(v.costPerOption, 10)
      that.addCash(-am)
    }
    if (v.numberOfSharesCall < 0) {
      let haveShares = this.getTotalUnitsOfStockCall(v.type)
      v.numberOfSharesCall = -v.numberOfSharesCall
      if (v.numberOfSharesCall > haveShares) {
        v.numberOfSharesCall = haveShares
      }
      while (v.numberOfSharesCall > 0) {
        Object.keys(this.stocks).forEach(function (key) {
          if (that.stocks[key].type === v.type) {
            let a = v.numberOfSharesCall
            if (that.stocks[key].numberOfSharesCall < a) {
              a = that.stocks[key].numberOfSharesCall
            }
            let am = a * (-parseInt(that.stocks[key].costPerShare, 10) + parseInt(v.costPerOption, 10))
            that.addCash(am)
            if (a === that.stocks[key].numberOfSharesCall) {
              delete that.stocks[key]
            } else {
              that.stocks[key].numberOfSharesCall -= a
            }
            v.numberOfSharesCall -= a
          }
        })
      }
    }
  }

  @action.bound addStockPut (v) {
    let that = this
    v.numberOfSharesPut = parseInt(v.numberOfSharesPut, 10)
    if (v.numberOfSharesPut > 0) {
      let uuid = generateUuid()
      this.stocks[uuid] = v
      this.changeHack = Math.random()
      let am = v.numberOfSharesPut * parseInt(v.costPerOption, 10)
      that.addCash(-am)
    }
    if (v.numberOfSharesPut < 0) {
      let haveShares = this.getTotalUnitsOfStockPut(v.type)
      v.numberOfSharesPut = -v.numberOfSharesPut
      if (v.numberOfSharesPut > haveShares) {
        v.numberOfSharesPut = haveShares
      }
      while (v.numberOfSharesPut > 0) {
        Object.keys(this.stocks).forEach(function (key) {
          if (that.stocks[key].type === v.type) {
            let a = v.numberOfSharesPut
            if (that.stocks[key].numberOfSharesPut < a) {
              a = that.stocks[key].numberOfSharesPut
            }
            let am = a * (parseInt(that.stocks[key].costPerShare) - parseInt(v.costPerOption, 10))
            that.addCash(am)
            if (a === that.stocks[key].numberOfSharesPut) {
              delete that.stocks[key]
            } else {
              that.stocks[key].numberOfSharesPut -= a
            }
            v.numberOfSharesPut -= a
          }
        })
      }
    }
  }

  @action.bound NextTurn () {
    let that = this
    Object.keys(this.stocks).forEach(function (key) {
      if (that.stocks[key].turnsLeft > 1) {
        that.stocks[key].turnsLeft--
        that.changeHack = Math.random()
      } else if (that.stocks[key].turnsLeft <= 1) {
        delete that.stocks[key]
        that.changeHack = Math.random()
      }
    })
  }

  @action.bound addBusinessFastTrack (v) {
    let uuid = generateUuid()
    let l = 0
    let that = this
    let exists = null
    if (!this.businessesFastTrackEmpty) {
      console.log('test1')
      Object.keys(this.businessesFastTrack).forEach(function (key) {
        if (that.businessesFastTrack[key].title.substr(0, 6) === v.title.substr(0, 6)) {
          exists = key
        }
      })
      if (exists) {
        console.log('test2', that.businessesFastTrack[exists].title, that.businessesFastTrack[exists].cashFlow)
        that.businessesFastTrack[exists].franchiseAmount = that.businessesFastTrack[exists].franchiseAmount + 1
        that.businessesFastTrack[exists].down = that.businessesFastTrack[exists].down + v.downPay
        that.businessesFastTrack[exists].cashFlow = that.businessesFastTrack[exists].cashFlow + v.cashFlow
        l++
      } else {
        console.log('test3')
        that.businessesFastTrack[uuid] = v
        that.businessesFastTrack[uuid].title = v.title
        that.businessesFastTrack[uuid].down = v.downPay
        that.businessesFastTrack[uuid].cashFlow = v.cashFlow
        that.businessesFastTrack[uuid].franchiseAmount = 1
      }
    } else {
      console.log('test4')
      this.businessesFastTrack[uuid] = v
      this.businessesFastTrack[uuid].title = v.title
      this.businessesFastTrack[uuid].down = v.downPay
      this.businessesFastTrack[uuid].cashFlow = v.cashFlow
      this.businessesFastTrack[uuid].franchiseAmount = 1
    }
    this.changeHack = Math.random()
    l++
    return this.changeHack > 0 && l > 0
  }

  @action.bound addDream (v) {
    let uuid = generateUuid()
    this.dreamsBought[uuid] = v
    this.changeHack = Math.random()
  }

  // Needed for "sell" dialog fast track

  // @action.bound sellBusinessFastTrack (uuid, amount) {
  //   let that = this
  //   Object.keys(this.businessesFastTrack).forEach(function (key) {
  //     if (key === uuid) {
  //       that.addCash(parseInt(amount, 10) - parseInt(that.businessesFastTrack[key].mortgage, 10))
  //       delete that.businessesFastTrack[key]
  //     }
  //   })
  //   this.changeHack = Math.random()
  // }

  @action.bound addBusiness (v) {
    let uuid = generateUuid()
    this.businesses[uuid] = v
    this.changeHack = Math.random()
  }

  @action.bound addCashflowToBusiness (uuid, amount) {
    let that = this
    Object.keys(this.businesses).forEach(function (key) {
      if (key === uuid) {
        that.businesses[key].cashFlow = parseInt(that.businesses[key].cashFlow, 10) + parseInt(amount, 10)
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound increaseSpareTimeCashflow (amount) {
    let that = this
    Object.keys(this.businesses).forEach(function (key) {
      if (that.businesses[key].type === 'Spare Time Co.') {
        that.businesses[key].cashFlow = parseInt(that.businesses[key].cashFlow, 10) + parseInt(amount, 10)
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound sellBusiness (uuid, amount) {
    let that = this
    Object.keys(this.businesses).forEach(function (key) {
      if (key === uuid) {
        that.addCash(parseInt(amount, 10) - parseInt(that.businesses[key].mortgage, 10))
        delete that.businesses[key]
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound sellBusinessesFastTrack (uuid, amount) {
    let that = this
    Object.keys(this.businessesFastTrack).forEach(function (key) {
      if (key === uuid) {
        that.addCash(parseInt(amount, 10))
        delete that.businessesFastTrack[key]
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound sellPartnership (uuid, factor) {
    let that = this
    Object.keys(this.businesses).forEach(function (key) {
      if (key === uuid) {
        that.addCash(parseInt(that.businesses[key].cost, 10) * factor)
        delete that.businesses[key]
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound addLiability (v) {
    let uuid = generateUuid()
    this.liabilities[uuid] = v
    this.changeHack = Math.random()
  }

  @action.bound addOtherAsset (v) {
    let uuid = generateUuid()
    this.otherAssets[uuid] = v
    this.changeHack = Math.random()
  }

  @action.bound sellOtherAsset (uuid, sellUnits, amount) {
    let that = this
    Object.keys(this.otherAssets).forEach(function (key) {
      if (key === uuid) {
        sellUnits = parseInt(sellUnits, 10)
        if (sellUnits > that.otherAssets[key].units) {
          sellUnits = that.otherAssets[key].units
        }
        that.addCash(parseInt(amount, 10) * sellUnits)
        if (sellUnits < that.otherAssets[key].units) {
          that.otherAssets[key].units -= sellUnits
        } else {
          delete that.otherAssets[key]
        }
      }
    })
    this.changeHack = Math.random()
  }

  @action.bound deleteAllOtherAssets () {
    this.stocks = {}
    this.changeHack = Math.random()
  }

  @action.bound deleteAllStocks () {
    this.stocks = {}
    this.changeHack = Math.random()
  }

  @action.bound deleteAllRealEstates () {
    this.realEstates = {}
    this.changeHack = Math.random()
  }

  @action.bound deleteAllBusinesses () {
    this.businesses = {}
    this.changeHack = Math.random()
  }

  @action.bound deleteAllLiabilities () {
    this.liabilities = {}
    this.changeHack = Math.random()
  }

  @action.bound splitStock (tp, fact) {
    if (!this.stocksEmpty) {
      for (let st in this.stocks) {
        if (this.stocks[st].type === tp) {
          this.stocks[st].numberOfShares = parseInt(this.stocks[st].numberOfShares * fact, 10)
          this.stocks[st].numberOfSharesShort = parseInt(this.stocks[st].numberOfSharesShort * fact, 10)
          this.stocks[st].numberOfSharesCall = parseInt(this.stocks[st].numberOfSharesCall * fact, 10)
          this.stocks[st].numberOfSharesPut = parseInt(this.stocks[st].numberOfSharesPut * fact, 10)
          this.stocks[st].costPerShare = parseFloat(this.stocks[st].costPerShare) / fact
          this.stocks[st].costPerOption = parseFloat(this.stocks[st].costPerOption) / fact
        }
      }
    }
    this.changeHack = Math.random()
  }

  /*
  --------------------------------------------------------------------------
  */
  @computed get cashFastTrack () {
    return this.cash + this.passiveIncome * 100 + this.cashFastTrackExtra
  }

  @computed get stocksEmpty () {
    return this.changeHack > 0 && Object.keys(this.stocks).length === 0
  }

  @computed get realEstatesEmpty () {
    return this.changeHack > 0 && Object.keys(this.realEstates).length === 0
  }

  @computed get businessesEmpty () {
    return this.changeHack > 0 && Object.keys(this.businesses).length === 0
  }

  @computed get businessesFastTrackEmpty () {
    return this.changeHack > 0 && Object.keys(this.businessesFastTrack).length === 0
  }

  @computed get dreamsEmpty () {
    return this.changeHack > 0 && Object.keys(this.dreamsBought).length === 0
  }
  @computed get dreamsTwoOrMore () {
    return this.changeHack > 0 && Object.keys(this.dreamsBought).length >= 2
  }

  @computed get otherAssetsEmpty () {
    return this.changeHack > 0 && Object.keys(this.otherAssets).length === 0
  }

  @computed get liabilitiesEmpty () {
    return this.changeHack > 0 && Object.keys(this.liabilities).length === 0
  }

  @computed get turnsIndicator () {
    let that = this
    let l = 0
    Object.keys(this.stocks).forEach(function (key) {
      if (that.stocks[key].turnsLeft > l) {
        l = that.stocks[key].turnsLeft
      }
    })
    return l * (parseInt(this.changeHack, 10) + 1)
  }

  @computed get hasSpareTime1 () {
    let that = this
    let l = 0
    Object.keys(this.businesses).forEach(function (key) {
      if (that.businesses[key].type === 'Spare Time Co.') {
        l++
      }
    })
    return this.changeHack > 0 && l > 0
  }

  @computed get hasSpareTime2 () {
    let that = this
    let l = 0
    Object.keys(this.businesses).forEach(function (key) {
      if (that.businesses[key].type === 'Spare Time Co.' && that.businesses[key].cashFlow > 0) {
        l++
      }
    })
    return this.changeHack > 0 && l > 0
  }

  @computed get dividendsEmpty () {
    let that = this
    let l = 0
    Object.keys(this.stocks).forEach(function (key) {
      if (!isNaN(parseFloat(that.stocks[key].dividendPerMonth))) {
        l++
      }
    })
    return this.changeHack > 0 && l === 0
  }

  @computed get dividends () {
    let that = this
    let r = {}
    if (!this.dividendsEmpty) {
      Object.keys(this.stocks).forEach(function (key) {
        if (!isNaN(parseFloat(that.stocks[key].dividendPerMonth))) {
          r[key] = that.stocks[key]
        }
      })
    }
    return r
  }

  @computed get interestsEmpty () {
    let that = this
    let l = 0
    Object.keys(this.stocks).forEach(function (key) {
      if (!isNaN(parseFloat(that.stocks[key].interestPerMonth))) {
        l++
      }
    })
    return this.changeHack > 0 && l === 0
  }

  @computed get interests () {
    let that = this
    let r = {}
    if (!this.interestsEmpty) {
      Object.keys(this.stocks).forEach(function (key) {
        if (!isNaN(parseFloat(that.stocks[key].interestPerMonth))) {
          r[key] = that.stocks[key]
        }
      })
    }
    return r
  }

  @computed get royaltiesEmpty () {
    let that = this
    let l = 0
    Object.keys(this.otherAssets).forEach(function (key) {
      if (that.otherAssets[key].type2 === 'royalty') {
        l++
      }
    })
    return this.changeHack > 0 && l === 0
  }

  @computed get royalties () {
    let that = this
    let r = {}
    if (!this.royaltiesEmpty) {
      Object.keys(this.otherAssets).forEach(function (key) {
        if (that.otherAssets[key].type2 === 'royalty') {
          r[key] = that.otherAssets[key]
        }
      })
    }
    return r
  }

  @computed get bankLoanPayment () {
    return parseInt(this.bankLoan / 10, 10)
  }

  @computed get boatLoanPayment () {
    return parseInt(this.boatLoan / 50, 10)
  }

  @computed get oldBankLoanPayment () {
    return parseInt(this.oldState.bankLoan / 10, 10)
  }

  @computed get oldBoatLoanPayment () {
    return parseInt(this.oldState.boat / 10, 10)
  }

  @computed get isOutOfRatRace () {
    return (this.cashflowVersion === '101' && this.passiveIncome >= this.totalExpenses) ||
      (this.cashflowVersion === '202' && this.passiveIncome >= 2 * this.totalExpenses)
  }

  @computed get isWon () {
    let ret = false
    if (this.cashflowVersion === '101') {
      if (this.extraPassiveIncomeFastTrack >= 50000) {
        ret = true
      }
      if (!this.dreamsEmpty) {
        Object.keys(this.dreamsBought).forEach(key => {
          if (this.dreamsBought[key].title === this.dream) {
            ret = true
          }
        })
      }
    }
    if (!this.dreamsEmpty) {
      Object.keys(this.dreamsBought).forEach(key => {
        if (this.cashflowVersion === '202' && this.extraPassiveIncomeFastTrack >= 50000) {
          if (this.dreamsBought[key].title === this.dream || this.dreamsTwoOrMore) {
            ret = true
          }
        }
      })
    }
    return ret && this.changeHack > 0
  }

  @computed get passiveIncomeFastTrack () {
    return (100 * this.passiveIncome) + this.extraPassiveIncomeFastTrack
  }

  getTotalUnitsOfStock (v) {
    let that = this
    let r = 0
    if (!this.stocksEmpty) {
      Object.keys(this.stocks).forEach(function (key) {
        if (that.stocks[key].type === v && that.stocks[key].numberOfShares > 0) {
          r += that.stocks[key].numberOfShares
        }
      })
    }
    return r
  }

  getTotalUnitsOfStockShort (v) {
    let that = this
    let r = 0
    if (!this.stocksEmpty) {
      Object.keys(this.stocks).forEach(function (key) {
        if (that.stocks[key].type === v && that.stocks[key].numberOfSharesShort > 0) {
          r += that.stocks[key].numberOfSharesShort
        }
      })
    }
    return r
  }

  getTotalUnitsOfStockCall (v) {
    let that = this
    let r = 0
    if (!this.stocksEmpty) {
      Object.keys(this.stocks).forEach(function (key) {
        if (that.stocks[key].type === v && that.stocks[key].numberOfSharesCall > 0) {
          r += that.stocks[key].numberOfSharesCall
        }
      })
    }
    return r
  }

  getTotalUnitsOfStockPut (v) {
    let that = this
    let r = 0
    if (!this.stocksEmpty) {
      Object.keys(this.stocks).forEach(function (key) {
        if (that.stocks[key].type === v && that.stocks[key].numberOfSharesPut > 0) {
          r += that.stocks[key].numberOfSharesPut
        }
      })
    }
    return r
  }

  getRareCoins () {
    let that = this
    let r = {}
    if (!this.otherAssetsEmpty) {
      Object.keys(this.otherAssets).forEach(function (key) {
        if (that.otherAssets[key].type === 'Royal Spanish Coin') {
          r[key] = {...that.otherAssets[key]}
          r[key].name = that.otherAssets[key].type
          r[key].chk = false
        }
      })
    }
    return r
  }

  getKrugers () {
    let that = this
    let r = {}
    if (!this.otherAssetsEmpty) {
      Object.keys(this.otherAssets).forEach(function (key) {
        if (that.otherAssets[key].type === 'One-ounce Krugerrands') {
          r[key] = {...that.otherAssets[key]}
          r[key].name = that.otherAssets[key].type
          r[key].chk = false
        }
      })
    }
    return r
  }

  getSmallBusinesses () {
    let that = this
    let r = {}
    if (!this.businessesEmpty) {
      Object.keys(this.businesses).forEach(function (key) {
        if (that.businesses[key].type === 'Small Business - Software' || that.businesses[key].type === 'Small Business - Widgets') {
          r[key] = {...that.businesses[key]}
          r[key].name = that.businesses[key].type
          r[key].chk = false
        }
      })
    }
    return r
  }

  getPartnerships () {
    let that = this
    let r = {}
    if (!this.businessesEmpty) {
      Object.keys(this.businesses).forEach(function (key) {
        if (that.businesses[key].type.substr(0, 14) === 'Partnership - ') {
          r[key] = {...that.businesses[key]}
          r[key].name = that.businesses[key].type
          r[key].chk = false
        }
      })
    }
    return r
  }

  getBizSofware () {
    return this.getBizX('Small Business - Software')
  }

  getBizWidget () {
    return this.getBizX('Small Business - Widgets')
  }

  getBizBed () {
    return this.getBizX('Bed & Breakfast')
  }

  getBizMall () {
    return this.getBizX('Small Shopping Mall')
  }

  getBizCar () {
    let that = this
    let r = {}
    if (!this.businessesEmpty) {
      Object.keys(this.businesses).forEach(function (key) {
        if (that.businesses[key].type === 'Car Wash' || that.businesses[key].type === 'Auto Wash') {
          r[key] = {...that.businesses[key]}
          r[key].name = that.businesses[key].type
          r[key].chk = false
        }
      })
    }
    return r
  }

  getBizX (v) {
    let that = this
    let r = {}
    if (!this.businessesEmpty) {
      Object.keys(this.businesses).forEach(function (key) {
        if (that.businesses[key].type === v) {
          r[key] = {...that.businesses[key]}
          r[key].name = that.businesses[key].type
          r[key].chk = false
        }
      })
    }
    return r
  }

  getHouses (v) {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'house' && that.realEstates[key].units === v) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getRentals (v) {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'house' || that.realEstates[key].type === 'condo' || that.realEstates[key].type === 'plex') {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getRecessionRentals (v) {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'house' || that.realEstates[key].type === 'condo' || that.realEstates[key].type === 'plex') {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
          let u = that.realEstates[key].units
          if (that.realEstates[key].type === 'house' || that.realEstates[key].type === 'condo') {
            u = 1
          }
          r[key].cashFlow -= 50 * u
        }
      })
    }
    return r
  }

  getRealEstateWithHighestCashflow () {
    let that = this
    let r = {}
    let maxCashflow = -99999999
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].cashFlow > maxCashflow) {
          maxCashflow = that.realEstates[key].cashFlow
        }
      })
      let a = 0
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].cashFlow === maxCashflow) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = a === 0
          a++
        }
      })
    }
    return r
  }

  getLand (v) {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'land' && that.realEstates[key].units === v) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getLandMin (v) {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'land' && that.realEstates[key].units >= v) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getApartments () {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'plex' && that.realEstates[key].units > 8) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getPlexes2or4or8 () {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'plex' && that.realEstates[key].units <= 8) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getPlexes (v) {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'plex' && that.realEstates[key].units === v) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getPlexes12up () {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'plex' && that.realEstates[key].units >= 12) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getPlexes24up () {
    let that = this
    let r = {}
    if (!this.realEstatesEmpty) {
      Object.keys(this.realEstates).forEach(function (key) {
        if (that.realEstates[key].type === 'plex' && that.realEstates[key].units >= 24) {
          r[key] = {...that.realEstates[key]}
          r[key].name = that.getRealEstateName(that.realEstates[key])
          r[key].chk = false
        }
      })
    }
    return r
  }

  getRealEstateName (v) {
    if (v.type === 'plex') {
      if (v.units === 2) {
        return 'Duplex'
      } else if (v.units >= 12) {
        return v.units + 'u apartment'
      } else {
        return v.units + '-plex'
      }
    }
    if (v.type === 'house') {
      if (v.units === 2 && this.cashflowVersion === '101') {
        return '2/1 condo'
      }
      return v.units + '/' + (v.units - 1) + ' house'
    }
    if (v.type === 'land') {
      return v.units + ' Acres of Land'
    }
    return ''
  }

  /*
  --------------------------------------------------------------------------
  */

  getState () {
    let o = JSON.parse(JSON.stringify(this))
    delete o.default
    return o
  }

  resetAllData () {
    let v = JSON.parse(JSON.stringify(toJS(new Store({}))))
    this.setState(v)
  }

  setState (v) {
    let that = this
    if (v) {
      Object.keys(v).forEach(function (key) {
        that[key] = v[key]
      })
    }
  }

  @action.bound finishedState (v) {
    if (v) {
      this.oldState = {...v}
      this.changeHack = Math.random()
    }
  }

  backupToLocalStorage (v) {
    v = {...v}
    delete v.default
    // console.log('backup', v)
    localStorage.setItem('cashflow', JSON.stringify(v))
  }

  restoreFromLocalStorage () {
    try {
      let v = JSON.parse(localStorage.getItem('cashflow'))
      // console.log('v', v)
      this.setState(v)
    } catch (e) {
      console.log('No restoreFromLocalStorage:', e)
    }
  }

  /*
  --------------------------------------------------------------------------
  */

  @computed get totalIncome () {
    return this.salary + this.passiveIncome
  }

  @computed get passiveIncome () {
    let v = this.changeHack * 0

    if (!this.realEstatesEmpty) {
      for (let re in this.realEstates) {
        let vv = parseFloat(this.realEstates[re].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    }

    if (!this.businessesEmpty) {
      for (let re in this.businesses) {
        let vv = parseFloat(this.businesses[re].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    }

    if (!this.stocksEmpty) {
      for (let st in this.stocks) {
        let vv = parseFloat(this.stocks[st].dividendPerMonth)
        if (!isNaN(vv)) {
          v += vv * this.stocks[st].numberOfShares
        }
      }
    }

    if (!this.stocksEmpty) {
      for (let st in this.stocks) {
        let vv = parseFloat(this.stocks[st].interestPerMonth)
        if (!isNaN(vv)) {
          v += vv * this.stocks[st].numberOfShares
        }
      }
    }

    this.otherAssets && Object.keys(this.otherAssets).forEach((key) => {
      if (this.otherAssets[key].type2 === 'royalty') {
        let vv = parseFloat(this.otherAssets[key].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    })

    return v
  }

  @computed get oldPassiveIncome () {
    let v = this.changeHack * 0

    if (!this.oldState.realEstatesEmpty) {
      for (let re in this.oldState.realEstates) {
        let vv = parseFloat(this.oldState.realEstates[re].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    }

    if (!this.oldState.businessesEmpty) {
      for (let re in this.oldState.businesses) {
        let vv = parseFloat(this.oldState.businesses[re].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    }

    if (!this.oldState.businessesFastTrackEmpty) {
      for (let re in this.oldState.businessesFastTrack) {
        let vv = parseFloat(this.oldState.businessesFastTrack[re].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    }

    if (!this.oldState.stocksEmpty) {
      for (let st in this.oldState.stocks) {
        let vv = parseFloat(this.oldState.stocks[st].dividendPerMonth)
        if (!isNaN(vv)) {
          v += vv * this.oldState.stocks[st].numberOfShares
        }
      }
    }

    if (!this.oldState.stocksEmpty) {
      for (let st in this.oldState.stocks) {
        let vv = parseFloat(this.oldState.stocks[st].interestPerMonth)
        if (!isNaN(vv)) {
          v += vv * this.oldState.stocks[st].numberOfShares
        }
      }
    }

    return v
  }

  @computed get extraPassiveIncomeFastTrack () {
    let v = this.changeHack * 0

    if (!this.businessesFastTrackEmpty) {
      for (let re in this.businessesFastTrack) {
        let vv = parseFloat(this.businessesFastTrack[re].cashFlow)
        if (!isNaN(vv)) {
          v += vv
        }
      }
    }

    return v
  }

  @computed get totalExpenses () {
    let v = this.taxes +
    this.homeMortgagePayment +
    this.schoolLoanPayment +
    this.carPayment +
    this.creditcardPayment +
    this.retailPayment +
    this.otherExpenses +
    this.boatLoanPayment +
    this.bankLoanPayment +
    (this.numberOfChildren * this.perChildExpense)

    if (!this.liabilitiesEmpty) {
      for (let liability in this.liabilities) {
        v += this.liabilities[liability].monthlyExpenses
      }
    }

    return Math.round(v + this.changeHack / 99999999)
  }

  @computed get oldTotalExpenses () {
    let v = this.oldState.taxes +
    this.oldState.homeMortgagePayment +
    this.oldState.schoolLoanPayment +
    this.oldState.carPayment +
    this.oldState.creditcardPayment +
    this.oldState.retailPayment +
    this.oldState.otherExpenses +
    this.oldState.boatLoanPayment +
    this.oldBankLoanPayment +
    (this.oldState.numberOfChildren * this.oldState.perChildExpense)

    if (!this.oldState.liabilitiesEmpty) {
      for (let liability in this.oldState.liabilities) {
        v += this.oldState.liabilities[liability].monthlyExpenses
      }
    }

    return Math.round(v + this.changeHack / 99999999)
  }

  @computed get monthlyCashFlow () {
    return this.totalIncome - this.totalExpenses
  }

  @computed get oldCash () {
    return this.oldState.cash
  }
}

/*
--------------------------------------------------------------------------
--------------------------------------------------------------------------
*/

function uuidv4 () {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ window.crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}

// Convert GUID string to Base-64 in Javascript
// by Mark Seecof, 2012-03-31

// GUID string with four dashes is always MSB first,
// but base-64 GUID's vary by target-system endian-ness.
// Little-endian systems are far more common.  Set le==true
// when target system is little-endian (e.g., x86 machine).
//
function guidToBase64 (g, le) {
  var hexlist = '0123456789abcdef'
  var b64list = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&%'

  var s = g.replace(/[^0-9a-f]/ig, '').toLowerCase()
  if (s.length !== 32) return ''

  if (le) {
    s = s.slice(6, 8) + s.slice(4, 6) + s.slice(2, 4) + s.slice(0, 2) +
    s.slice(10, 12) + s.slice(8, 10) +
    s.slice(14, 16) + s.slice(12, 14) +
    s.slice(16)
  }
  s += '0'

  var a, p, q
  var r = ''
  var i = 0
  while (i < 33) {
    a = (hexlist.indexOf(s.charAt(i++)) << 8) |
      (hexlist.indexOf(s.charAt(i++)) << 4) |
      (hexlist.indexOf(s.charAt(i++)))

    p = a >> 6
    q = a & 63

    r += b64list.charAt(p) + b64list.charAt(q)
  }
  // r += '=='

  return r
} // guid_to_base64()

function generateUuid () {
  var newId = '%'
  var uuid = ''
  // var tries = 0
  while (newId.indexOf('%') >= 0 || newId.indexOf('&') >= 0) {
    uuid = uuidv4()
    newId = guidToBase64(uuid)
    // tries++
  }
  // console.log('tries', tries)
  // console.log('uuidv4', uuid)
  // console.log('new id', newId)

  return newId
}

/*
--------------------------------------------------------------------------
--------------------------------------------------------------------------
*/

let store = new Store({})

store.restoreFromLocalStorage()

autorun(_ => {
  // console.log('store changed', toJS(store))
  store.backupToLocalStorage(toJS(store))
})

export default store
