
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', name: 'home', component: () => import('pages/index') },
      { path: 'statement', name: 'statement', component: () => import('pages/statement'), meta: { title: 'Statement' } },
      { path: 'fastTrack', name: 'fastTrack', component: () => import('pages/fast-track'), meta: { title: 'Fast Track' } },
      { path: 'debug', name: 'debug', component: () => import('pages/debug'), meta: { title: 'Debug' } },
      { path: 'help', name: 'help', component: () => import('pages/help'), meta: { title: 'Help' } },
      { path: 'bugreport', name: 'bugreport', component: () => import('pages/bug-report'), meta: { title: 'Bug Report' } }
    ]
  },

  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
